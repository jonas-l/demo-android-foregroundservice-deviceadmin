package org.inkompetenz.fgsdademo

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.core.content.getSystemService

class ForegroundService: Service() {
    override fun onBind(p0: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()

        val notificationManager = getSystemService<NotificationManager>()!!

        notificationManager.createNotificationChannel(
            NotificationChannel("ch", "demo channel", NotificationManager.IMPORTANCE_LOW)
        )

        startForeground(
            1,
            Notification.Builder(this, "ch")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentText("FG Service")
                .setForegroundServiceBehavior(Notification.FOREGROUND_SERVICE_IMMEDIATE)
                .build()
        )
    }
}